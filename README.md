# Lola 3mm version #

Este repositorio contiene los diseños de todas las piezas que componen el robot lola en cualquiera de sus variantes.
Puedes encontrar los archivos para fabricación dentro de la carpeta: "Archivos Generados"

**Repositorios relaciones** 
* https://bitbucket.org/padrinotecnolgico/lola_ui Interfaz gráfica 

### ¿Como usar este repositorio? ###

* Esto es un repositorio GIT. Puedes clonarlo en tu ordenador como cualquier otro repo git.
* Si no quieres emplear un cliente git, puedes descargar el repositorio en la sección descargas de la izquierda. Ahí encontrarás:
    * "Download repository": descargas la última version del repositorio
    * TAGS: Descargar una versión específica. Normalmente cada vez que se realizan cambios siginificativos y estos son validados, se crea una nueva versión
    * Branches: Sis se está desarrollando una nueva funcionalidad, pero esta aún no está estabilizada e incluida en la versión principal, podrás encontrarla en su rama correspondiente.
*Si quieres descargar un solo archivo, usa la sección de "source" te permite ver el contenido del repositorio. Cuando encuentres el archivo que estabas buscando, abrelo y usa la opción raw para descarlo

* Si quieres enviar cambios al repositorio, mandalos como pull request. Si no empleas un cliente de git, mandaselos a Javier Murcia, junto con la descripción de los cambios, para que los incluya en el repositorio.

### Esquema de directorios ###
* **Archivos generados**
    **Este es el directorio que buscas**
    Este directorio contiene los archivos listos para ser fabricados. Está organizado en función de la tecnología de fabricación, y contiene archvios que han sido producidos a partir de los originales que se encuentran en el repositorio
    * **En el caso los archivos para impresión 3d:**
        * Archivos **STL**, listos para ser importados en cualquier slicer
        * Archivos GCODE, listos para ser impresos, simplemente copiandolos en la tarjeta SD. Estos archivos son para **Bq Witbox**. Encontrarás 2 versiones, en función de para que boquilla se ha generado el archivo.
            * : BW_04: Para impresoras con boquilla de 0.4mm (la estandard)
            * : BW_08: Para impresoras con boquilla de 0.8mm (la negra de de fabricación), que imprime más rápido
* **Modelos auxiliares**
    Ignorar este directorio. Contiene los modelos 3d de las piezas manufacturadas que compramos. Únicamente se usan en el ensamblado para porder ver el la versión completa del robot.
* **Cara Lola**
    Los archivos para el soporte de la pantalla táctil, y el embellecedor
* **Mariquita**
    Archivos para fabricar el caparazon de la mariquita
* **Piezas impresas**
    Piezas de la plataforma base que se imprimen en 3d. Por ejemplo, la caja de la botonera trasera
* **Plataforma base**
    Las piezas de chapa de aluminio cortadas.

### Otra información relevante ###

Versión de inventor: 2017
Propietario del proyecto:
* Padrino Tecnológico

Responsable del repositorio (por si necesitas ayuda):
* Javier Murcia
* Padrino Tecnológico, DS101
* Extensión: 6683
* Mail: javi@islautopia.com